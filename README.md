# Molinos

## General intention
A few scripts which will be useful to produce kml output.

## Files
- test files consisting of:
    - the 'files' folder mainly with icons and images from field move
    - a strab and a field-move kml files
    - output.kml file

- executables
    - Atm the 'dip.py' file is the only executable script. 

## dip.py 
### Has to be called as follows

> dip.py strabo-file-1.kml ... strabo-file-xyz.kml field-move.kml > output.kml

The file field-move.kml is copied to the generated output and serves as a blueprint for the insertion of strabospot data.



