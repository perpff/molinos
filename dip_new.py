#!/usr/bin/env python3

import sys
import copy

if len(sys.argv) < 3:
    print ('You have to provide (1) the strabo file, (2) the field move file as inputs')
    exit(0)

args = sys.argv[1:-1]

diplist = [] #(dip, dipdirection, id)

for arg in args:
    
    try:
        with open(arg) as file:
            text = file.readlines()
    except:
        print(f'Could not open file {arg}')
        exit(0)

    file_length = len(text)

    arg = arg.rsplit('.')[0]

    placemarklist = ['<Placemark>' in buf for buf in text]
    placemarkendlist = ['</Placemark>' in buf for buf in text]

    placemark_lines = []
    placemarkend_lines = []
    for num, val in enumerate(placemarklist):
        if (val):
            placemark_lines.append(num)

    for num, val in enumerate(placemarkendlist):
        if (val):
            placemarkend_lines.append(num)

    count = 0

    for num_b, num_e in zip(placemark_lines, placemarkend_lines):
        
        num_b = num_b + 1
        num_e = num_e - 1

        # first check if there are ANY coordinates in this section
        orientation_data = []
        coords = ()
        if '<Point><coordinates>' in text[num_e]:
            coords = text[num_e].split('>')
            coords = coords[2].split('<')
            coords = coords[0].split(',')
            coords = (coords[0], coords[1])

        # if there are coordinates, find Planar sections
        if len(coords):
            for i in range(num_b, num_e):
                if '<div class="sectionTitle">Planar Orientation: </div>' in text[i]:
                    orientation_data.append(i)

            # if there is orientation data, append to list
            if len(orientation_data):
                orientation_data.append(num_e)

                for plane_b, plane_e in zip(orientation_data[:-1], orientation_data[1:]):

                    dd = 1000
                    d = 1000
                    
                    # style for bedding
                    fault = False

                    for k in range(plane_b, plane_e):
                        if 'Dip Direction:' in text[k]:
                            dd = int(text[k].rsplit(': ')[1].split('<')[0])
                        
                        if 'Dip: ' in text[k]:
                            d = int(text[k].rsplit(': ')[1].split('<')[0])

                        if 'Fault' in text[k]:
                            fault = True
                            

                    if dd != 1000 and d != 1000:

                        idn = str(count) + "_" + arg
                        count = count + 1
                        diplist.append((str(d).zfill(2), str(dd).zfill(3), coords, idn, fault))
        else:
            continue


# copy the old kml file (the one that field move provides)
with open(sys.argv[-1]) as file:
    text = file.read().splitlines()

# print beginning of the field-move file
for i in text[:-3]:
    print (i)

# print new placemarks / points
for i in diplist:
    print(f'\t\t<Placemark id="{i[3]}placemark">')
    print(f'\t\t\t<name>{i[0]}°</name>')
    print('\t\t\t<open>1</open>')
    print(f'\t\t\t<Snippet maxLines="2">{i[0]}° / {i[1]}°</Snippet>')
    print(f'\t\t\t<description>{i[0]}° / {i[1]}°')
    print('X')
    
    if i[4]: # fault
        print('Fault\n\n')
    else:
        print('Plane Type 1\n\n')
        
    print('Declination: 2.99°</description>')
    print('\t\t\t<TimeStamp></TimeStamp>')
    
    if i[4]: # fault
        print('\t\t\t<styleUrl>#1140</styleUrl>')
    else:
        print('\t\t\t<styleUrl>#115260</styleUrl>')
    
    print('\t\t\t<Style>')
    print('\t\t\t\t<IconStyle>')
    print(f'\t\t\t\t\t<heading>{i[1]}</heading>')
    print('\t\t\t\t</IconStyle>')
    print('\t\t\t</Style>')
    print(f'\t\t\t<Point id="{i[3]}point">')
    print(f'\t\t\t\t<coordinates>{i[2][0]},{i[2][1]},0</coordinates>')
    print('\t\t\t</Point>')
    print('\t\t</Placemark>')

# print ending of the field-move file
for i in text[-3:]:
    print(i)

