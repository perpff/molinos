#!/usr/bin/env python3

import sys
from os import path
import copy
import urllib.request
import random

if len(sys.argv) < 3:
    print ('You have to provide (1) the strabo file, (2) the field move file as inputs')
    exit(0)

args = sys.argv[1:-1]

photolist = [] #(dip, dipdirection, id)

for arg in args:
    
    try:
        with open(arg) as file:
            text = file.readlines()
    except:
        print(f'Could not open file {arg}')
        exit(0)

    file_length = len(text)

    arg = arg.rsplit('.')[0]

    placemarklist = ['<Placemark>' in buf for buf in text]
    placemarkendlist = ['</Placemark>' in buf for buf in text]

    placemark_lines = []
    placemarkend_lines = []
    for num, val in enumerate(placemarklist):
        if (val):
            placemark_lines.append(num)

    for num, val in enumerate(placemarkendlist):
        if (val):
            placemarkend_lines.append(num)

    count = 0

    for num_b, num_e in zip(placemark_lines, placemarkend_lines):
        num_b = num_b + 1
        num_e = num_e - 1

        # first check if there are ANY coordinates in this section
        photo_refs = []
        coords = ()

        count_images = 0
        
        for i in range(num_b, num_e):   
            if '<a href="https://www.strabospot.org/geimage/' in text[i]:
                count_images += 1

            if '<coordinates>' in text[i]:
                coords = text[i].split('<coordinates>')[1]
                coords = coords.split('</coordinates>')[0]
                coords = coords.split(',')
                coords = (coords[0], coords[1])

        # if there are coordinates, find photos
        if len(coords):
            for i in range(num_b, num_e):
                if '<div>Image Type: Photo</div>' in text[i]:
                    photo_refs.append(i)

            # if there are references to photos, append to list
            if len(photo_refs):
                photo_refs.append(num_e)

                for photo_b, photo_e in zip(photo_refs[:-1], photo_refs[1:]):

                    url = ''
                    azimuth = 1000
                    
                    for k in range(photo_b, photo_e):
                        if 'View Azimuth Trend:' in text[k]:
                            azimuth = int(text[k].rsplit('View Azimuth Trend: ')[1].split('</')[0])
                        
                        if '<a href="https://www.strabospot.org/geimage/' in text[k]:
                            url = text[k].rsplit('<div><a href="')[1].split('"><img src')[0]
                            url = url.replace('geimage', 'mapimage') + '.jpg'

                    if len(url):
                        idn = str(count) + "_" + arg
                        count = count + 1
                        style = 232 #if count_images else 2320
                        photolist.append([azimuth, url, coords, idn, style])
        else:
            continue

# copy the old kml file (the one that field move provides)
with open(sys.argv[-1]) as file:
    text = file.read().splitlines()

'''
print beginning of the field-move file
exchange icon / heading

check below placemarks, if the string 'Heading:' appears
if not, exclude the placemark
'''
placemarklist_b = []
placemarklist_e = []
for i, line in enumerate(text[:-3]):
    if '<Placemark id=' in line:
        placemarklist_b.append(i)
    if '</Placemark>' in line:
        placemarklist_e.append(i)

for b, e in zip(reversed(placemarklist_b), reversed(placemarklist_e)):
    
    image_with_heading = False

    for i in range(b,e):
        if 'Heading: ' in text[i]:
            image_with_heading = True
            break

    if not image_with_heading:
       del text[b:e+1]

in_placemark = False
for i, line in enumerate(text[:-3]):

    # toggle switch
    if '<Placemark id=' in line:
        in_placemark = True
    if '</Placemark>' in line:
        in_placemark = False

    if in_placemark:
        if 'Heading: ' in line:
            heading = int(line.rsplit(' ')[1].rsplit('.')[0])

        if '<name>' in line:
            line = '<name></name>'
       
        if '<styleUrl>' in line and in_placemark:
            print('\t\t\t\t<Style>')
            print('\t\t\t\t\t<IconStyle>')
            print(f'\t\t\t\t\t\t<heading>{heading}</heading>')
            print('\t\t\t\t\t\t<Icon><href>files/direction_arrow.png</href></Icon>')
            print('\t\t\t\t\t\t<scale>0.75</scale>')
            print('\t\t\t\t\t\t<hotspot x="0.5" y="0.0" xunits="fraction" yunits="fraction"></hotspot>')
            print('\t\t\t\t\t\t<gx:headingMode>northUp</gx:headingMode>')
            print('\t\t\t\t\t</IconStyle>')
            print('\t\t\t\t</Style>')
        else:
            print (line)
    else:
        print (line)

# we want to download the image files, and then rename them 
# this has obviously to be done only once
for i, photo in enumerate(photolist):
    url = photo[1]
    filename = 'files/' + url.split('/')[-1].replace('.jpg', '_big.jpg') 
    photo.append(filename)
    if not path.exists(filename):
        print (url, filename, i)
        urllib.request.urlretrieve(url, filename)

print('\t\t<Folder id="Strabofolder">')
print('\t\t\t<name>Strabofolder</name>')

# print new placemarks / points
# photolist.append([azimuth, url, coords, idn, filename, style])
for photo in photolist:

    print(f'\t\t\t<Placemark id="{photo[3] + "placemark"}">')
    print('\t\t\t\t<name></name>')
    print('<Snippet maxLines="0"></Snippet>')
    print(f'<description><![CDATA[<img src="{photo[5]}" height="300" /><br /><br />')

    if photo[0] == 1000:
        print(f'Heading: ?')
        print('\nDeclination: 2.99°]]></description>')
        print('\t\t\t\t<TimeStamp></TimeStamp>')
        print('\t\t\t\t<Style>')
        print('\t\t\t\t\t<IconStyle>')
        print('\t\t\t\t\t\t<Icon><href>files/outcrop_mark.png</href></Icon>')
        print('\t\t\t\t\t\t<scale>0.5</scale>')
        print('\t\t\t\t\t\t<hotspot x="0.5" y="0.0" xunits="fraction" yunits="fraction"></hotspot>')
        print('\t\t\t\t\t</IconStyle>')
        print('\t\t\t\t</Style>')

    else:
        print(f'Heading: {photo[0]}')
        print('\nDeclination: 2.99°]]></description>')
        print('\t\t\t\t<TimeStamp></TimeStamp>')
        print('\t\t\t\t<Style>')
        print('\t\t\t\t\t<IconStyle>')
        print(f'\t\t\t\t\t\t<heading>{photo[0]}</heading>')
        #print(f'\t\t\t\t\t\t<heading>{random.randint(0,359)}</heading>')
        print('\t\t\t\t\t\t<Icon><href>files/direction_arrow.png</href></Icon>')
        print('\t\t\t\t\t\t<scale>0.75</scale>')
        print('\t\t\t\t\t\t<hotspot x="0.5" y="0.0" xunits="fraction" yunits="fraction"></hotspot>')
        print('\t\t\t\t\t\t<gx:headingMode>northUp</gx:headingMode>')
        print('\t\t\t\t\t</IconStyle>')
        print('\t\t\t\t</Style>')

    print(f'\t\t\t\t<Point id="{photo[3]}">')
    print(f'\t\t\t\t\t<coordinates>{photo[2][0]},{photo[2][1]},0</coordinates>')
    print('\t\t\t\t</Point>')
    print('\t\t\t</Placemark>')

print('\t\t</Folder>')

# print ending of the field-move file
for i in text[-3:]:
    print(i)

