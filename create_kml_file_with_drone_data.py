#!/usr/bin/env python3

import sys
from os import path
import copy
import urllib.request
import random

arg = sys.argv[1]
    
try:
    with open(sys.argv[1]) as file:
        text = file.readlines()
except:
    print(f'Could not open file {arg}')
    exit(0)

text = text[0]
text = text.split('<name>')

photo_list = []

for i, val in enumerate(text):
    if '<latitude>' in val and '</name>' in val and '<longitude>' in val: # first check what we have
        buf = val.split('<')
        name = buf[0]
        lat = buf[3].split('>')[1]
        lon = buf[5].split('>')[1]
        photo_list.append((name, lat, lon))
    else:
        continue
    
# copy the old kml file (the one that field move provides)
with open('doc_fieldmove.kml') as file:
    text = file.read().splitlines()

print('<?xml version="1.0" encoding="UTF-8"?>')
print('<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">')

print('<Document>')
print('\t<name>Drone images and movies</name>')

print('\t\t<Folder id="Dronephotos">')
print('\t\t\t<name>Drone photosr</name>')

# print new placemarks / points
# photolist.append([azimuth, url, coords, idn, filename, style])
for photo in photo_list:

    print(f'\t\t\t<Placemark id="{photo[0] + "placemark"}">')
    print('\t\t\t\t<name></name>')
    print('\t\t\t\t<Snippet maxLines="0"></Snippet>')
    print(f'\t\t\t\t<description><![CDATA[<img src="photos/{photo[0]}" height="300" /><br /><br /> See here: files/{photo[0]}]]></description>')

    print('\t\t\t\t<TimeStamp></TimeStamp>')
    print('\t\t\t\t<Style>')
    print('\t\t\t\t\t<IconStyle>')
    print('\t\t\t\t\t\t<Icon><href>photos/outcrop_mark.png</href></Icon>')
    print('\t\t\t\t\t\t<scale>0.5</scale>')
    print('\t\t\t\t\t\t<hotspot x="0.5" y="0.5" xunits="fraction" yunits="fraction"></hotspot>')
    print('\t\t\t\t\t</IconStyle>')
    print('\t\t\t\t</Style>')

    print(f'\t\t\t\t<Point id="{photo[0]}">')
    print(f'\t\t\t\t\t<coordinates>{photo[1]},{photo[2]},0</coordinates>')
    print('\t\t\t\t</Point>')
    print('\t\t\t</Placemark>')

# print ending of the field-move file
print('\t\t</Folder>')
print('\t</Document>')
print('</kml>')

