#!/usr/bin/env python3

import sys
import copy

#print (sys.argv)
#print(len(sys.argv))

if len(sys.argv) < 3:
    print ('You have to provide (1) the strabo file, (2) the field move file as inputs')
    exit(0)

args = sys.argv[1:-1]

diplist = [] #(dip, dipdirection, id)

for arg in args:
    
    try:
        with open(arg) as file:
            text = file.readlines()
    except:
        print('Could not open file')
        exit(0)

    file_length = len(text)

    arg = arg.rsplit('.')[0]
    count = 0

    # in the strabo kml files, the Dip is saved one line after the Dip Direction
    dipdir = ['Dip Direction:' in buf for buf in text]

    for num, val in enumerate(dipdir):
        if (val):
            try:
                idn = str(count) + "_" + arg
                dd = int(text[num].rsplit(': ')[1].split('<')[0])
                d = int(text[num+1].rsplit(': ')[1].split('<')[0])

                # now search for the point location
                #<Point><coordinates>

                for i in range(num+2, file_length):
                    if '<Point><coordinates>' in text[i]:
                        coords = text[i].split('>')
                        coords = coords[2].split('<')
                        coords = coords[0].split(',')
                        coords = (coords[0], coords[1])

                        break

                count = count + 1

                diplist.append((str(d).zfill(2), str(dd).zfill(3), coords, idn))
            except:
                continue

# copy the old kml file (the one that field move provides)
with open(sys.argv[-1]) as file:
    text = file.read().splitlines()

# print beginning of the field-move file
for i in text[:-3]:
    print (i)

# print new placemarks / points
for i in diplist:
    print(f'\t\t<Placemark id="{i[3]}placemark">')
    print(f'\t\t\t<name>{i[0]}°</name>')
    print('\t\t\t<open>1</open>')
    print(f'\t\t\t<Snippet maxLines="2">{i[0]}° / {i[1]}°</Snippet>')
    print(f'\t\t\t<description>{i[0]}° / {i[1]}°')
    print('X')
    print('Plane Type 1\n\n')
    print('Declination: 2.99°</description>')
    print('\t\t\t<TimeStamp></TimeStamp>')
    print('\t\t\t<styleUrl>#115260</styleUrl>')
    print('\t\t\t<Style>')
    print('\t\t\t\t<IconStyle>')
    print(f'\t\t\t\t\t<heading>{i[1]}</heading>')
    print('\t\t\t\t</IconStyle>')
    print('\t\t\t</Style>')
    print(f'\t\t\t<Point id="{i[3]}point">')
    print(f'\t\t\t\t<coordinates>{i[2][0]},{i[2][1]},0</coordinates>')
    print('\t\t\t</Point>')
    print('\t\t</Placemark>')

# print ending of the field-move file
for i in text[-3:]:
    print(i)

